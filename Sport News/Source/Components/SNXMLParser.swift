//
//  SNXMLParser.swift
//  Sport News
//
//  Created by Pedro Moitinho on 20/11/17.
//

import UIKit

enum SNXMLParserResponse {
    case success([[String: AnyObject]])
    case error(Error?)
}

class SNXMLParser: NSObject {
    
    var elementKey: String = ""
    var valueKeys: [String] = []
    
    var results: [[String: AnyObject]]? = []
    var currentDictionary: [String: AnyObject]!
    var currentValue: String?
    var error: Error?
    
    func parseData(data: Data, elementKey: String, valueKeys: [String], completion: @escaping (SNXMLParserResponse) -> ()) {
        
        self.elementKey = elementKey
        self.valueKeys = valueKeys
        
        let parser = XMLParser(data: data)
        parser.delegate = self
        if parser.parse(), let results = results {
            completion(.success(results))
        } else {
            completion(.error(error))
        }
    }
}

extension SNXMLParser: XMLParserDelegate {
    
    func parser(_ parser: XMLParser, didStartElement elementName: String, namespaceURI: String?, qualifiedName qName: String?, attributes attributeDict: [String : String] = [:]) {
        
        if elementName == elementKey {
            
            self.currentDictionary = [String : AnyObject]()
            
            if attributeDict.count > 0 {
                self.currentDictionary = attributeDict as [String : AnyObject]
            }
        } else if valueKeys.contains(elementName) {
            
            self.currentValue = String()
        } else if elementName == "enclosure" {
            
            let attrsUrl = attributeDict as [String: NSString]
            self.currentValue = attrsUrl["url"] as String?
        }
    }
    
    func parser(_ parser: XMLParser, foundCharacters string: String) {
        
        self.currentValue? += string
    }
    
    func parser(_ parser: XMLParser, didEndElement elementName: String, namespaceURI: String?, qualifiedName qName: String?) {
        
        if elementName == self.elementKey {
            
            self.results?.append(self.currentDictionary)
            self.currentDictionary = nil
        } else if valueKeys.contains(elementName), self.currentDictionary != nil {
            
            self.currentDictionary[elementName] = currentValue as AnyObject
            self.currentValue = nil
        } else if elementName == "enclosure", self.currentDictionary != nil {
            
            self.currentDictionary[elementName] = currentValue as AnyObject
            self.currentValue = nil
        }
    }
    
    func parser(_ parser: XMLParser, parseErrorOccurred parseError: Error) {
        
        self.currentValue = nil
        self.currentDictionary = nil
        self.results = nil
        self.error = parseError
    }
}
