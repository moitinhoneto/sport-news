//
//  HomeViewController.swift
//  Sport News
//
//  Created by Pedro Moitinho on 16/11/17.
//

import UIKit

class HomeViewController: UIViewController {
    
    // MARK: - Private Properties
    
    @IBOutlet private weak var menuHeightConstraint: NSLayoutConstraint!
    @IBOutlet private weak var menuTopConstraint: NSLayoutConstraint!
    @IBOutlet private weak var showMenuButton: UIButton!
    
    private var isShowingMenu: Bool {
        return menuTopConstraint.constant != 0
    }
    
    private var containerTabBarController: UITabBarController? {
        return childViewControllers.flatMap{ $0 as? UITabBarController }.first
    }
    
    // MARK: - ViewController
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        containerTabBarController?.tabBar.isHidden = true
        showMenu(false, animated: false)
    }
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    // MARK: - Action Methods

    @IBAction private func didTouchMenuButton(_ sender: Any) {
        
        showMenu(isShowingMenu)
    }
    
    @IBAction private func didTouchNewsButton(_ sender: Any) {
        
        containerTabBarController?.selectedIndex = 0
        showMenu(false)
    }
    
    @IBAction private func didTouchScoresButton(_ sender: Any) {
        
        containerTabBarController?.selectedIndex = 1
        showMenu(false)
    }
    
    @IBAction private func didTouchStandingsButton(_ sender: Any) {
        
        containerTabBarController?.selectedIndex = 2
        showMenu(false)
    }
    
    // MARK: - Private Methods
    
    private func showMenu(_ show: Bool, animated: Bool = true) {
        
        menuTopConstraint.constant = show ? 0 : -menuHeightConstraint.constant
        if animated {
            UIView.animate(withDuration: 0.3) {
                self.showMenuButton.backgroundColor = show ? UIColor(named: "BlueMenu") : UIColor.black
                self.view.layoutIfNeeded()
            }
        } else {
            showMenuButton.backgroundColor = show ? UIColor(named: "BlueMenu") : UIColor.black
            view.layoutIfNeeded()
        }
    }
    
    // MARK: - Public Methods
}
