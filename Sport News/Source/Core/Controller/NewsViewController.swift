//
//  NewsViewController.swift
//  Sport News
//
//  Created by Pedro Moitinho on 16/11/17.
//

import UIKit
import SafariServices

class NewsViewController: UIViewController {

    // MARK: - Private Properties
    
    @IBOutlet private weak var tableView: UITableView!
    
    private lazy var refreshControl: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(handleRefresh(_:)), for: UIControlEvents.valueChanged)

        return refreshControl
    }()
    
    private var newsArray: [News] = []
    
    // MARK: - ViewController
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.addSubview(self.refreshControl)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        if newsArray.count == 0 {
            refreshControl.refreshManually()
        }
    }
    
    // MARK: - Action Methods
    
    @objc private func handleRefresh(_ refreshControl: UIRefreshControl) {
        
        fetchData()
    }
    
    // MARK: - Private Methods
    
    private func fetchData() {
        
        NewsBusiness.fetchNews { response in
            
            DispatchQueue.main.async {
                switch response {
                case .success(let newsArray):
                    self.newsArray = newsArray
                    
                case .error( _):
                    self.newsArray = []
                    
                    let alertController: UIAlertController = UIAlertController(title: "Error", message: nil, preferredStyle: .alert)
                    let okAction: UIAlertAction = UIAlertAction(title: "Ok", style: .cancel) { _ in
                    }
                    alertController.addAction(okAction)
                    self.present(alertController, animated: true)
                }
                
                self.tableView.reloadData()
                self.refreshControl.endRefreshing()
            }
        }
    }
}

// MARK: - UITableViewDataSource, UITableViewDelegate
extension NewsViewController: UITableViewDataSource, UITableViewDelegate {

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return newsArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        guard let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: NewsTableViewCell.self), for: indexPath) as? NewsTableViewCell else {
            return UITableViewCell()
        }
        
        cell.titleLabel.text = newsArray[indexPath.row].title
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "EEEE, dd MMMM yyyy, hh:mm"
        
        cell.dateLabel.text = dateFormatter.string(from: newsArray[indexPath.row].pubDate)
        
        SNProvider.imageRequest(url: newsArray[indexPath.row].imageURL) { url, image in
            if self.newsArray[indexPath.row].imageURL == url {
                cell.newsImageView.image = image
            }
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let controller = SFSafariViewController(url: newsArray[indexPath.row].link)
        controller.delegate = self
        present(controller, animated: true)
    }
}

// MARK: - SFSafariViewControllerDelegate
extension NewsViewController: SFSafariViewControllerDelegate {
    
    func safariViewControllerDidFinish(_ controller: SFSafariViewController) {
        controller.dismiss(animated: true)
    }
}
