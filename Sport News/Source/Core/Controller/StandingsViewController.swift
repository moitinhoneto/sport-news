//
//  StandingsViewController.swift
//  Sport News
//
//  Created by Pedro Moitinho on 16/11/17.
//

import UIKit

class StandingsViewController: UIViewController {
    
    // MARK: - Private Properties
    
    @IBOutlet private weak var tableView: UITableView!
    
    lazy private var refreshControl: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(handleRefresh(_:)), for: UIControlEvents.valueChanged)
        
        return refreshControl
    }()
    
    private var clubsArray: [Club] = []
    
    // MARK: - ViewController
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.addSubview(self.refreshControl)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        
        if clubsArray.count == 0 {
            refreshControl.refreshManually()
        }
    }
    
    // MARK: - Action Methods
    
    @objc private func handleRefresh(_ refreshControl: UIRefreshControl) {
        
        fetchData()
    }
    
    // MARK: - Private Methods
    
    private func fetchData() {
        
        StandingsBusiness.fetchClubs { response in
            
            DispatchQueue.main.async {
                switch response {
                case .success(let clubsArray):
                    self.clubsArray = clubsArray
                    
                case .error( _):
                    self.clubsArray = []
                    
                    let alertController: UIAlertController = UIAlertController(title: "Error", message: nil, preferredStyle: .alert)
                    let okAction: UIAlertAction = UIAlertAction(title: "Ok", style: .cancel) { _ in
                    }
                    alertController.addAction(okAction)
                    self.present(alertController, animated: true)
                }
                
                self.tableView.reloadData()
                self.refreshControl.endRefreshing()
            }
        }
    }
}

// MARK: - UITableViewDataSource
extension StandingsViewController: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return clubsArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        guard let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: StandingsTableViewCell.self), for: indexPath) as? StandingsTableViewCell else {
            return UITableViewCell()
        }
        
        cell.rankLabel.text = String(format: "%02d", clubsArray[indexPath.row].rank)
        cell.nameLabel.text = clubsArray[indexPath.row].name
        cell.pLabel.text = String(format: "%02d", clubsArray[indexPath.row].matchesTotal)
        cell.wLabel.text = String(format: "%02d", clubsArray[indexPath.row].matchesWon)
        cell.dLabel.text = String(format: "%02d", clubsArray[indexPath.row].matchesDraw)
        cell.lLabel.text = String(format: "%02d", clubsArray[indexPath.row].matchesLost)
        cell.gdLabel.text = String(format: "%02d", clubsArray[indexPath.row].goalsPro)
        cell.ptsLabel.text = String(format: "%02d", clubsArray[indexPath.row].points)
        
        cell.backgroundColor = indexPath.row % 2 != 0 ? UIColor.white : UIColor(named: "GreyCell")
        return cell
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        guard let header = tableView.dequeueReusableCell(withIdentifier: String(describing: StandingsTableViewHeader.self)) as? StandingsTableViewHeader else {
            return nil
        }
        
        return header
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 35
    }
    
}
