//
//  ScoresViewController.swift
//  Sport News
//
//  Created by Pedro Moitinho on 16/11/17.
//

import UIKit

class ScoresViewController: UIViewController {

    // MARK: - Private Properties
    
    @IBOutlet private weak var tableView: UITableView!
    
    private var timer: Timer?
    private var scoresArray: [Score] = []
    
    // MARK: - ViewController
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        fetchData()
        startUpdaterTimer()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        
        stopUpdaterTimer()
    }
    
    // MARK: - Private Methods
    
    @objc private func fetchData() {
        
        ScoresBusiness.fetchMatches { response in
            
            DispatchQueue.main.async {
                switch response {
                case .success(let scoresArray):
                    self.scoresArray = scoresArray
                case .error( _):
                    self.scoresArray = []
                }
                
                self.tableView.reloadData()
            }
        }
    }
    
    private func startUpdaterTimer() {
        
        if timer == nil {
            timer = Timer.scheduledTimer(timeInterval: 30, target: self, selector: #selector(self.fetchData), userInfo: nil, repeats: true)
        }
    }
    
    private func stopUpdaterTimer() {
        if timer != nil {
            timer?.invalidate()
            timer = nil
        }
    }
}

// MARK: - UITableViewDataSource
extension ScoresViewController: UITableViewDataSource, UITableViewDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return scoresArray.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return scoresArray[section].matches.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        guard let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: ScoresTableViewCell.self), for: indexPath) as? ScoresTableViewCell else {
            return UITableViewCell()
        }
        
        cell.teamALabel.text = scoresArray[indexPath.section].matches[indexPath.row].teamA
        cell.teamBLabel.text = scoresArray[indexPath.section].matches[indexPath.row].teamB
        cell.scoreLabel.text = String(format: "%d - %d", scoresArray[indexPath.section].matches[indexPath.row].fsA, scoresArray[indexPath.section].matches[indexPath.row].fsB)
        
        cell.backgroundColor = indexPath.row % 2 != 0 ? UIColor.white : UIColor(named: "GreyCell")
        return cell
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        guard let header = tableView.dequeueReusableCell(withIdentifier: String(describing: ScoresTableViewHeader.self)) as? ScoresTableViewHeader else {
            return nil
        }
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd MMMM yyyy"
        
        header.dateLabel.text = dateFormatter.string(from: scoresArray[section].date)
        
        return header
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 35
    }
    
}
