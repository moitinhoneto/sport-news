//
//  RefreshControllExtension.swift
//  Sport News
//
//  Created by Pedro Moitinho on 20/11/17.
//

import UIKit

extension UIRefreshControl {
    
    func refreshManually() {
        if let scrollView = superview as? UIScrollView {
            scrollView.setContentOffset(CGPoint(x: 0, y: scrollView.contentOffset.y - frame.height), animated: false)
        }
        beginRefreshing()
        sendActions(for: .valueChanged)
    }
}
