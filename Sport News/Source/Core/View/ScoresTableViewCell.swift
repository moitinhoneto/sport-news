//
//  ScoresTableViewCell.swift
//  Sport News
//
//  Created by Pedro Moitinho on 16/11/17.
//

import UIKit

class ScoresTableViewCell: UITableViewCell {
    
    @IBOutlet weak var teamALabel: UILabel!
    @IBOutlet weak var teamBLabel: UILabel!
    @IBOutlet weak var scoreLabel: UILabel!
    @IBOutlet weak var gradientView: UIView! {
        didSet {
            let gradient = CAGradientLayer()
            
            gradient.frame = gradientView.bounds
            gradient.colors = [UIColor.lightGray.cgColor, UIColor.darkGray.cgColor]
            
            gradientView.layer.insertSublayer(gradient, at: 0)
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

}
