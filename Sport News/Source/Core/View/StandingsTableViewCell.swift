//
//  StandingsTableViewCell.swift
//  Sport News
//
//  Created by Pedro Moitinho on 16/11/17.
//

import UIKit

class StandingsTableViewCell: UITableViewCell {

    @IBOutlet weak var rankLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var pLabel: UILabel!
    @IBOutlet weak var wLabel: UILabel!
    @IBOutlet weak var dLabel: UILabel!
    @IBOutlet weak var lLabel: UILabel!
    @IBOutlet weak var gdLabel: UILabel!
    @IBOutlet weak var ptsLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
}
