//
//  SNProvider.swift
//  Sport News
//
//  Created by Pedro Moitinho on 20/11/17.
//

import UIKit

enum SNProviderResponse {
    case success(Data)
    case error(Error?)
}

struct SNProvider {
    
    static func request(url: URL, completion: @escaping (SNProviderResponse) -> ()) {
        
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        
        let task = URLSession.shared.dataTask(with: request) { data, response, error in
            
            if let data = data, error == nil {
                completion(.success(data))
            } else {
                completion(.error(error))
            }
        }
        
        task.resume()
    }
    
    static func imageRequest(url: URL, completion: @escaping (URL, UIImage?) -> ()) {
        
        let task = URLSession.shared.dataTask(with: url) { data, response, error in
            
            DispatchQueue.main.async {
                if let data = data, error == nil {
                    completion(url, UIImage(data: data))
                } else {
                    completion(url ,nil)
                }
            }
        }
        
        task.resume()
    }
}
