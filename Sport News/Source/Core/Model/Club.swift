//
//  Standings.swift
//  Sport News
//
//  Created by Pedro Moitinho on 20/11/17.
//

import Foundation

struct Club {
    
    private let kName = "club_name"
    private let kRank = "rank"
    private let kMatchesTotal = "matches_total"
    private let kMatchesWon = "matches_won"
    private let kMatchesDraw = "matches_draw"
    private let kMatchesLost = "matches_lost"
    private let kGoalsPro = "goals_pro"
    private let kGoalsAgainst = "goals_against"
    private let kPoints = "points"
    
    let name: String
    let rank: Int
    let matchesTotal: Int
    let matchesWon: Int
    let matchesDraw: Int
    let matchesLost: Int
    let goalsPro: Int
    let goalsAgainst: Int
    let points: Int
    
    init?(dictionary: [String: AnyObject]) {
        
        guard let name = dictionary[kName] as? String else { return nil }
        guard let rank = dictionary[kRank] as? String, let rankInt = Int(rank) else { return nil }
        guard let matchesTotal = dictionary[kMatchesTotal] as? String, let matchesTotalInt = Int(matchesTotal) else { return nil }
        guard let matchesWon = dictionary[kMatchesWon] as? String, let matchesWonInt = Int(matchesWon) else { return nil }
        guard let matchesDraw = dictionary[kMatchesDraw] as? String, let matchesDrawInt = Int(matchesDraw) else { return nil }
        guard let matchesLost = dictionary[kMatchesLost] as? String, let matchesLostInt = Int(matchesLost) else { return nil }
        guard let goalsPro = dictionary[kGoalsPro] as? String, let goalsProInt = Int(goalsPro) else { return nil }
        guard let goalsAgainst = dictionary[kGoalsAgainst] as? String, let goalsAgainstInt = Int(goalsAgainst) else { return nil }
        guard let points = dictionary[kPoints] as? String, let pointsInt = Int(points) else { return nil }
        self.name = name
        self.rank = rankInt
        self.matchesTotal = matchesTotalInt
        self.matchesWon = matchesWonInt
        self.matchesDraw = matchesDrawInt
        self.matchesLost = matchesLostInt
        self.goalsPro = goalsProInt
        self.goalsAgainst = goalsAgainstInt
        self.points = pointsInt
    }
}
