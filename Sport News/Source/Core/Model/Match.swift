//
//  Scores.swift
//  Sport News
//
//  Created by Pedro Moitinho on 20/11/17.
//

import Foundation

struct Match {
    
    private let kTeamA = "team_A_name"
    private let kTeamB = "team_B_name"
    private let kFsA = "fs_A"
    private let kFsB = "fs_B"
    private let kDate = "date_utc"
    
    let teamA: String
    let teamB: String
    let fsA: Int
    let fsB: Int
    let date: Date
    
    init?(dictionary: [String: AnyObject]) {
        
        guard let teamA = dictionary[kTeamA] as? String else { return nil }
        guard let teamB = dictionary[kTeamB] as? String else { return nil }
        guard let fsA = dictionary[kFsA] as? String, let fsAInt = Int(fsA) else { return nil }
        guard let fsB = dictionary[kFsB] as? String, let fsBInt = Int(fsB) else { return nil }
        guard let dateString = dictionary[kDate] as? String else { return nil }
        self.teamA = teamA
        self.teamB = teamB
        self.fsA = fsAInt
        self.fsB = fsBInt
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        guard let date = dateFormatter.date(from: dateString) else { return nil }
        self.date = date
    }
}
