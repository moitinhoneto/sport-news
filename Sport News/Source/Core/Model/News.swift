//
//  News.swift
//  Sport News
//
//  Created by Pedro Moitinho on 20/11/17.
//

import Foundation

struct News {
    
    private let kTitle = "title"
    private let kPubDate = "pubDate"
    private let kDescription = "description"
    private let kLink = "link"
    private let kEnclosure = "enclosure"
    
    let title: String
    let pubDate: Date
    let description: String
    let link: URL
    let imageURL: URL
    
    init?(dictionary: [String: AnyObject]) {
        
        guard let title = dictionary[kTitle] as? String else { return nil }
        guard let pubDateString = dictionary[kPubDate] as? String else { return nil }
        guard let description = dictionary[kDescription] as? String else { return nil }
        guard let link = dictionary[kLink] as? String, let linkURL = URL(string: link) else { return nil }
        guard let image = dictionary[kEnclosure] as? String, let imageURL = URL(string: image) else { return nil }
        self.title = title
        self.description = description
        self.link = linkURL
        self.imageURL = imageURL
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "EEE, dd MMM yyyy HH:mm:ss +zzzz"
        dateFormatter.locale = Locale(identifier: "en_GB")
        guard let pubDate = dateFormatter.date(from: pubDateString) else { return nil }
        self.pubDate = pubDate
    }
}
