//
//  ScoresBusiness.swift
//  Sport News
//
//  Created by Pedro Moitinho on 20/11/17.
//

import Foundation

enum ScoresBusinessResponse {
    case success([Score])
    case error(Error?)
}

typealias Score = (date: Date, matches: [Match])

struct ScoresBusiness {
    
    private static let urlString: String = "http://www.mobilefeeds.performgroup.com/utilities/interviews/techtest/scores.xml"
    
    static func fetchMatches(completion: @escaping (ScoresBusinessResponse)->()) {
        
        guard let url = URL(string: urlString) else {
            completion(ScoresBusinessResponse.error(nil))
            return
        }
        
        SNProvider.request(url: url) { requestResponse in
            
            switch requestResponse {
            case .success(let data):
                SNXMLParser().parseData(data: data, elementKey: "match", valueKeys: []) { parseResponse in
                    
                    switch parseResponse {
                    case .success(let items):
                        var matchesArray: [Match] = []
                        var datesArray: [Date] = []
                        for item in items {
                            if let match = Match(dictionary: item) {
                                matchesArray.append(match)
                                datesArray.append(match.date)
                            }
                        }
                        
                        var scoreArray: [Score] = []
                        for date in Set(datesArray) {
                            scoreArray.append((date: date, matches: matchesArray.filter{ $0.date == date }))
                        }
                        
                        completion(.success(scoreArray))
                        
                    case .error(let error):
                        completion(.error(error))
                    }
                }
            case .error(let error):
                completion(.error(error))
            }
        }
    }
    
}
