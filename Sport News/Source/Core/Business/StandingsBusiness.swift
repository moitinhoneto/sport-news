//
//  StandingsBusiness.swift
//  Sport News
//
//  Created by Pedro Moitinho on 20/11/17.
//

import Foundation

enum StandingsBusinessResponse {
    case success([Club])
    case error(Error?)
}

struct StandingsBusiness {
    
    private static let urlString: String = "http://www.mobilefeeds.performgroup.com/utilities/interviews/techtest/standings.xml"
    
    static func fetchClubs(completion: @escaping (StandingsBusinessResponse)->()) {
        
        guard let url = URL(string: urlString) else {
            completion(StandingsBusinessResponse.error(nil))
            return
        }
        
        SNProvider.request(url: url) { requestResponse in
            
            switch requestResponse {
            case .success(let data):
                SNXMLParser().parseData(data: data, elementKey: "ranking", valueKeys: []) { parseResponse in
                    
                    switch parseResponse {
                    case .success(let items):
                        var standingsArray: [Club] = []
                        for item in items {
                            if let standings = Club(dictionary: item) {
                                standingsArray.append(standings)
                            }
                        }
                        
                        standingsArray = standingsArray.sorted(by: { $0.rank < $1.rank })
                        
                        completion(.success(standingsArray))
                        
                    case .error(let error):
                        completion(.error(error))
                    }
                }
            case .error(let error):
                completion(.error(error))
            }
        }
    }
    
}
