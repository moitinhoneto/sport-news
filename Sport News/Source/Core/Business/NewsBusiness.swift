//
//  NewsBusiness.swift
//  Sport News
//
//  Created by Pedro Moitinho on 20/11/17.
//

import Foundation

enum NewsBusinessResponse {
    case success([News])
    case error(Error?)
}

struct NewsBusiness {
    
    private static let urlString: String = "http://www.mobilefeeds.performgroup.com/utilities/interviews/techtest/latestnews.xml"
    
    static func fetchNews(completion: @escaping (NewsBusinessResponse)->()) {
        
        guard let url = URL(string: urlString) else {
            completion(NewsBusinessResponse.error(nil))
            return
        }
        
        SNProvider.request(url: url) { requestResponse in
            
            switch requestResponse {
            case .success(let data):
                
                SNXMLParser().parseData(data: data, elementKey: "item", valueKeys: ["title", "pubDate", "description", "link"]) { parseResponse in
                    
                    switch parseResponse {
                    case .success(let items):
                        var newsArray: [News] = []
                        for item in items {
                            if let news = News(dictionary: item) {
                                newsArray.append(news)
                            }
                        }
                        
                        completion(.success(newsArray))
                        
                    case .error(let error):
                        completion(.error(error))
                    }
                }
                
            case .error(let error):
                completion(.error(error))
            }
        }
    }
    
}
